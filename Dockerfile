FROM ubuntu

RUN apt update
RUN apt install -y openjdk-8-jre-headless

RUN mkdir -p /home/java

COPY . /home/java

WORKDIR /home/java/

CMD [ "sh","script.sh" ]